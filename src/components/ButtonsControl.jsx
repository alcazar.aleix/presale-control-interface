import React, { useState , useEffect} from 'react';
import presaleABI from "./presaleABI.json"
import Web3 from "web3";
import './ButtonControl.css'
import {ProgressBar} from './ProgresBar.jsx'

let isInitialized = false;
let contractAddr;
let presaleIndex;
let erc20Contract;

export const ButtonsControl = () => {
    const [state, setState] = useState(0);
    const [numConfirmations, setNumConfirmations] = useState(0);
    const [error, setError] = useState("");
    const [actualWallet,setActualWallet] = useState("");
    const [presaleFinished,setPresaleFinished] = useState(false);
    const [introducedVal, setIntroducedVal] = useState("");
    const [amount, setAmount] = useState("");
    const [index, setIndex] = useState(0);
    const [forwardIndex, setForwardIndex] = useState(0);
    const [presaleIndex, setPresaleIndex] = useState(0);
    const [exec, setExec] = useState("");
    const [to, setTo] = useState("");
    const [contractBalance, setContractBalance] = useState("");
    const [raisedCap, setRaisedCap] = useState(false);
    const [txAlreadyConfirmed, setTxAlreadyConfirmed] = useState(false);
    const [walletAccount, setWalletAccount] = useState("");
    const [isConnected,setIsConnected] = useState("false");
    const [balanceBnb,setBalanceBnb] = useState(0);
    const updateProgress = (field, val) => {
      setState({ [field]: val });
    };
    useEffect(() => {
        handleConnectWallet();
        getBalance();
        getContractBalance();
        getActualWalletFunds();
        isPresaleFinished();
        getPresaleIndex();
        viewTx();
        getRiasedCap();
        updateProgress("percent", setState(raisedCap/1000));
        setIntroducedVal(introducedVal);
        isConfirmed();
    },[walletAccount,introducedVal,forwardIndex,raisedCap,numConfirmations])
    useEffect(() => {
      const interval = setInterval(() => {
        getRiasedCap();
      }, 2000);
      return () => clearInterval(interval);
    }, []);

    const handleSubmit = async (event) => {    
      event.preventDefault();
     alert(`The VAL you entered was: ${introducedVal}`)
      setIntroducedVal(introducedVal);

    }
    const  handleConnectWallet = async () => {
        const provider = await window.ethereum
        provider
            .request({ method: 'eth_requestAccounts' })
            .then((accounts) => {
              setWalletAccount(accounts[0])
              console.log(`Selected account is ${walletAccount}`);
            })        
            .catch((err) => {
              console.log(err);
              return;
            });
  
          window.ethereum.on('accountsChanged', function (accounts) {
            setWalletAccount(accounts[0])
            console.log(`Selected account changed to ${walletAccount}`);
          });

          const web3 = new Web3(provider);
          contractAddr= '0x0dfe392dC91C7F7534AFeC942393574e560ba3eF'//'0xEFDebe50372134CE1F5261B83825d82068a7A089';//presale contract
          const erc20Abi = presaleABI
          erc20Contract =  new web3.eth.Contract(
            erc20Abi,
            contractAddr
          );
  
          isInitialized = true;
  
      }
      const getBalance = async () => {
        const bal = await window.ethereum.request({method: 'eth_getBalance', params: [walletAccount , 'latest']})
        const wei = parseInt(bal,16)
        setBalanceBnb(wei / Math.pow(10, 18))
        console.log(balanceBnb)
      }
      const prepareForwardFunds = async () => {
        const provider = await window.ethereum
        provider
            .request({ method: 'eth_requestAccounts' })
            .then((accounts) => {
              setWalletAccount(accounts[0])
              console.log(`Selected account is ${walletAccount}`);
            })
            const web3 = new Web3(provider);
            contractAddr= '0x0dfe392dC91C7F7534AFeC942393574e560ba3eF';//presale contract
            const erc20Abi = presaleABI
            erc20Contract =  new web3.eth.Contract(
              erc20Abi,
              contractAddr
            );
        await erc20Contract.methods.prepareForward().send({from:walletAccount});
      }
      const getContractBalance = async () => {
        const bal = await window.ethereum.request({method: 'eth_getBalance', params: [contractAddr , 'latest']})
        const wei = parseInt(bal,16)
        setContractBalance(wei / Math.pow(10, 18))
      }
      const getRiasedCap = async () =>{
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.raisedCap().call().then((output)=>{
          setRaisedCap(output / Math.pow(10, 18));

        })
      }
      const getActualWalletFunds = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.walletFunds().call().then((output)=>{
          setActualWallet(output);
          console.log(actualWallet);
        });
      }

      const getPresaleIndex = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.presale_index().call().then((output)=>{
          setPresaleIndex(output);
          console.log(presaleIndex);
        });
      }
      const finalizePresale = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.finishPresale().send({from:walletAccount}).then((output)=>{
          console.log(output);
   
        });
      }
      const isPresaleFinished = async () =>{
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.finalized().call().then((output)=>{
          setPresaleFinished(output);
          console.log(output);
  
        });
      }
      const confWallet = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.confirmWallet().send({from:walletAccount}).then((output)=>{
          console.log(output);
  
        });
      }
      const settWallet = async (addressToSet) => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.setWallet(introducedVal.toString()).send({from:walletAccount}).then((output)=>{
          console.log(output);
  
        });
      }
      const confirmTx = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.confirmTransaction(introducedVal).send({from:walletAccount}).then((output)=>{
          console.log(output);
  
        });
      }
      const sendForwardFunds = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.forwardFunds(introducedVal).send({from:walletAccount}).then((output)=>{
          console.log(output);
  
        });
      }
      const initPause = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.pause().send({from:walletAccount}).then((output)=>{
          console.log(output);
  
        });
      }
      const stopPause = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.unpause().send({from:walletAccount}).then((output)=>{
          console.log(output);
  
        });
      
      }
      const isConfirmed = async () =>{
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.isConfirmed(introducedVal,walletAccount).call().then((output) => {
          setTxAlreadyConfirmed(output);
        });
      }
      const resetPresaleBalances = async () =>{
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.resetPresaleBalances().send({from: walletAccount});
      }
      
      const viewTx = async () => {
        if(!isInitialized){
          await handleConnectWallet();
        }
        await erc20Contract.methods.getTransaction(introducedVal).call().then((output)=>{
          setAmount(Web3.utils.fromWei(output[1]));
          setForwardIndex(parseInt(output[2])+1);
          setIndex(output[2]);
          setExec(output[3]);
          setTo(output[0]);
          setNumConfirmations(output["numConfirmations"]);
          console.log(output);
        }).catch((err) => {
          setError(err);
        });
      }
      const handleDisconnect = async () => {
        console.log("Disconnecting metamask")
        setIsConnected(false)
        setWalletAccount('')
      }
     
  return <div className='buttons'>
      <button onClick={handleConnectWallet}  className="main-connect-btn">{'Wallet: ' + walletAccount ? 'Wallet: ' + walletAccount : "CONNECT WALLET"}</button>
            
            {isConnected ? (
              <div className='buttons'>
              <button onClick={getBalance}  className="main-connect-btn">{'BNB Balance: ' +balanceBnb ? 'BNB Balance: ' + balanceBnb : "No BNB Balance"}</button>
              <button onClick={getContractBalance} onClick={prepareForwardFunds} className="main-connect-btn">{'Prepare ' + contractBalance + ' BNB to: ' + actualWallet + ' with Tx Index: ' + forwardIndex}</button>
              <button disabled={txAlreadyConfirmed} onClick={confirmTx} className="main-connect-btn">{'Confirm TX index: ' + index}</button>
              <button onClick={sendForwardFunds} className="main-connect-btn">{'Forward funds TX Index: ' + index}</button>
              <button onClick={initPause}  className="main-connect-btn">Init Pause</button>
              <button onClick={stopPause}  className="main-connect-btn">Stop Pause</button>
              <button onClick={settWallet} className="main-connect-btn">{actualWallet +': Click To Set New Wallet Funds'}</button>
              <button onClick={confWallet}  className="main-connect-btn">Confirm Selected Wallet</button>
              <button disabled={presaleFinished} onClick={finalizePresale}  className="main-connect-btn">{presaleFinished ? 'PRESALE FINISHED!' : 'Finish Presale'}</button>
              <button onClick={viewTx} className="main-connect-btn">{['GET TRANSACTION \n to: ' + to ,'\nindex: ' + index, '\namount: ' + amount, '\nexecuted: ' + exec + ' Num Confimations: ' + numConfirmations] ? ['GET TRANSACTION \n to: ' + to ,'\nindex: ' + index, '\namount: ' + amount, '\nexecuted: ' + exec +' Num.Confimations: ' + numConfirmations] : "View TX"}</button>
              <div className="text-insert">
              <form onSubmit={handleSubmit} >
                      <label>
                          <input  
                          type="number" 
                          value={introducedVal}
                          onChange={(e) => setIntroducedVal(e.target.value)}                                        
                          />
                      </label>
                 </form>
                </div>
                <ProgressBar width={600} percent={raisedCap/1500} />
                <button onClick={resetPresaleBalances}  className="main-connect-btn">Reset Presale Balances</button>
                <button onClick={handleDisconnect}  className="main-connect-btn">Disconnect</button>
                </div>
            ):(
              <div></div>
            )}
  </div>;
};
